# eSveikata_Redis
## Šis projektas paruoštas VU MIF 2 kurso studento Augustino Makevičiaus NoSQL duomenų bazių dalyko 1 užduočiai
## Projektas kurtas eSveikatos poertalo principu, kuriame galima registruotis pas gydytojus.
## Projektą sudaro dvi dalys: Paciento ir gydytojo.
### Pacientai
Pacientai registruojasi pas gydytojus, nurodydami jų kodą, tuomet pasirinkdami datą ir , jei gydytojas tą dieną dirba, vizito laiką. Gydytojų kodai pateikiami ligoninės interneto puslapyje.

Registruojantis pas gydytojus naudojamos transakcijos, tad commit leidžiama tik vienam pacientui, kitam išmetamas klaidos pranešimas.

Visito raktai saugomi formatu: "V-{docCode}-{date}-{time}", o šio rakto value tampa paciento kodu (naudojamas asmens kodas).

Pacientam reikia prisiregistruoti, tada jų duomenys saugomi ListRightPush komandos pagalba. Pacientam neleidžiama pas tą patį gydytoją eitu du kartus per dieną.

### Gydytojai
Šiai užduočiai nebuvo formuojama gydytojų prisijungimo sistema, tad bet koks gydytojo kodas ir slaptažodis yra priimtinas (bus panaudota set value, kad būtų galima pridėti kelis gydytojus).

Gydytojai prisijungę prie savo paskyros gali pridėti arba pašalinti darbos dienas ( saugomos formatu L-{docCode}-{date}, ListRightPush From, To) bei jas pašalinti.

Šalinant dienas naudojama transakciją, norint užtikrinti, kad pacientai negalėtų užsiregistruoti į trinamus vizitus, tačiau pacientai nėra informuojami, jei bus ištrinamas jau užpildytas vizitas.

## Naudingos nuorodos
https://stackexchange.github.io/StackExchange.Redis/

https://docs.microsoft.com/en-us/dotnet/framework/winforms/


