﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using StackExchange.Redis;

namespace SveikataRedis
{
    public partial class Form1 : Form
    {

        public void ClearDB()
        {
            ConnectionMultiplexer redis = ConnectionMultiplexer.Connect("localhost, allowAdmin=true");
            var server = redis.GetServer("localhost:6379");
            server.FlushAllDatabases();
        }

        ConnectionMultiplexer redis = ConnectionMultiplexer.Connect("localhost");
        public Form1()
        {
            //ClearDB();
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PatientForm p = new PatientForm(redis);
            p.ShowDialog();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DoctorForm d = new DoctorForm(redis);
            d.ShowDialog();
        }
    }
}
