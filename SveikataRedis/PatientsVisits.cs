﻿using System;
using System.Diagnostics;
using System.Windows.Forms;
using StackExchange.Redis;

namespace SveikataRedis
{
    public partial class PatientsVisits : Form
    {
        private IDatabase _db;
        private ConnectionMultiplexer _redis;
        private string _code;
        public PatientsVisits(string code, ConnectionMultiplexer redis)
        {
            _db = redis.GetDatabase();
            _redis = redis;
            _code = code;
            var name = _db.ListGetByIndex(code, 0);

            InitializeComponent();
            label2.Text = @"Welcome " + name;

            var server = _redis.GetServer("localhost: 6379");
            DateTime today = DateTime.Today;
            for (int i = 0; i <= 3; i++)
            {
                foreach (var key in server.Keys(pattern: $"V-*-{today.AddDays(i).ToShortDateString()}-*"))
                {
                    if(_db.StringGet(key) == _code) richTextBox1.Text += $"{key}\n";
                }
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            string docCode = textBox1.Text.Trim();
            string date = dateTimePicker1.Value.Date.ToShortDateString();
            if (_db.KeyExists(docCode))
            {
                if (_db.KeyExists($"L-{docCode}-{date}"))
                {
                    richTextBox2.Text = "";
                    var server = _redis.GetServer("localhost: 6379");
                    int c = 0;
                    foreach (var key in server.Keys(pattern: $"V-{docCode}-{date}-*"))
                    {
                        string val = _db.StringGet(key);
                        if (val == _code)
                        {
                            richTextBox2.Text = @"ERROR: You already have a visit on this day.";
                            return;
                        }
                        else if (val == "FREE")
                        {
                            richTextBox2.Text += $@"{key.ToString().Split('-')[3]}{' '}";
                            c++;
                        }
                    }

                    if (c != 0)
                    {
                        button2.Visible = true;
                        dateTimePicker2.Visible = true;
                    }
                    else richTextBox2.Text = @"ERROR: Doctors que is full on this day.";
                }
                else
                {
                    richTextBox2.Text = @"ERROR: Doctor does not work on this day.";
                }
            }
            else
            {
                richTextBox2.Text = @"ERROR: Incorrect doctor code.";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            bool valid = false;
            string docCode = textBox1.Text.Trim();
            string date = dateTimePicker1.Value.Date.ToShortDateString();
            string time = dateTimePicker2.Value.ToShortTimeString();
            if (_db.KeyExists($"V-{docCode}-{date}-{time}"))
            {
                if (_db.StringGet($"V-{docCode}-{date}-{time}") == "FREE")
                {
                    var transaction = _db.CreateTransaction();
                    transaction.AddCondition(Condition.KeyExists($"V-{docCode}-{date}-{time}"));
                    transaction.AddCondition(Condition.StringEqual($"V-{docCode}-{date}-{time}", "FREE"));
                    transaction.AddCondition(Condition.KeyExists($"L-{docCode}-{date}"));
                    transaction.StringSetAsync($"V-{docCode}-{date}-{time}", _code);
                    transaction.KeyExpireAsync($"V-{docCode}-{date}-{time}", dateTimePicker1.Value.AddDays(1));
                    bool commited = transaction.Execute();
                    if (commited)
                    {
                      richTextBox2.Text = @"Success";
                      richTextBox1.Text += $"V-{docCode}-{date}-{time}\n";
                    } 
                    else
                    {
                        richTextBox2.Text = @"ERROR: Time has been reserved or deleted. Try again.";
                    }
                    button2.Visible = false;
                    dateTimePicker2.Visible = false;
                    
                }
                else
                {
                    richTextBox2.Text += @"    ERROR: Time has just been taken.";
                }
            }
            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            else if(!valid)
            {
                richTextBox2.Text += @"    The time you put in is not valid";
                // ReSharper disable once RedundantAssignment
                valid = true;
            }
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string docCode = textBox2.Text.Trim();
            string date = dateTimePicker4.Value.Date.ToShortDateString();
            string time = dateTimePicker3.Value.ToShortTimeString();
            if (_db.KeyExists($"V-{docCode}-{date}-{time}") && _db.StringGet($"V-{docCode}-{date}-{time}") == _code)
            {
                _db.StringSet($"V-{docCode}-{date}-{time}", "FREE");
                richTextBox2.Text = "Please relog to see changes\n" + richTextBox2.Text;
            }
        }
    }
}
