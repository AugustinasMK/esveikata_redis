﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StackExchange.Redis;

namespace SveikataRedis
{
    public partial class DoctorForm : Form
    {
        private ConnectionMultiplexer _redis;
        private IDatabase _db;
        public DoctorForm(ConnectionMultiplexer redis)
        {
            _redis = redis;
            _db = redis.GetDatabase();
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            RedisValue pass = _db.StringGet(textBox1.Text.Trim());

            _db.StringSet(textBox1.Text.Trim(), textBox2.Text.Trim());
            DoctorVisits d = new DoctorVisits(_redis, textBox1.Text.Trim());
            d.ShowDialog();

            //if (pass == RedisValue.Null)
            //{
            //    Console.WriteLine("User does not exist.");
            //}
            //else
            //{
            //    if (pass == textBox2.Text.Trim())
            //    {
            //        DoctorVisits d = new DoctorVisits();
            //        d.ShowDialog();
            //    }
            //    else
            //    {
            //        Debug.WriteLine("Pass not match");
            //    }
            //}
        }
    }
}
