﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

using StackExchange.Redis;

namespace SveikataRedis
{
    public partial class PatientForm : Form
    {
        private ConnectionMultiplexer _redis;
        private IDatabase _db;
        public PatientForm(ConnectionMultiplexer redis)
        {
            _redis = redis;
            _db = redis.GetDatabase();
            InitializeComponent();
        }

        private void SIbut_Click(object sender, EventArgs e)
        {
            var isNumeric = long.TryParse(SIcode.Text.Trim(), out long n);
            if (isNumeric)
            {
                RedisValue pass = _db.ListGetByIndex(SIcode.Text.Trim(), 0);
                Debug.WriteLine(pass);
                pass = _db.ListGetByIndex(SIcode.Text.Trim(), 1);
                Debug.WriteLine(pass);
                pass = _db.ListGetByIndex(SIcode.Text.Trim(), 2);
                Debug.WriteLine(pass);
                if (pass == RedisValue.Null)
                {
                    Console.WriteLine("User does not exist.");
                }
                else
                {
                    if (pass == SIPass.Text.Trim())
                    {
                        PatientsVisits p = new PatientsVisits(SIcode.Text.Trim(), _redis);
                        p.ShowDialog();
                    }
                    else
                    {
                        Debug.WriteLine("Pass not match");
                    }
                }
            }
            else
            {
                Debug.WriteLine("Not numeric");
            }
        }

        private void SUbut_Click(object sender, EventArgs e)
        {
            var isNumeric = long.TryParse(SUcode.Text.Trim(), out long n);
            if (isNumeric)
            {
                if (SUpass1.Text == SUpass2.Text)
                {
                    RedisKey code = SUcode.Text.Trim();
                    _db.ListRightPush(code, SUname.Text.Trim());
                    _db.ListRightPush(code, SUmail.Text.Trim());
                    _db.ListRightPush(code, SUpass1.Text.Trim());
                    PatientsVisits p = new PatientsVisits(SUcode.Text.Trim(), _redis);
                    p.ShowDialog();
                }
            }
        }
    }
}
