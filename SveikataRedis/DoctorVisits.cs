﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using StackExchange.Redis;

namespace SveikataRedis
{
    public partial class DoctorVisits : Form
    {

        class WorkDay
        {
            public string date;
            public string FromTime;
            public string ToTime;
        }

        private ConnectionMultiplexer _redis;
        private IDatabase _db;
        private string _code;
        public DoctorVisits(ConnectionMultiplexer redis, string code)
        {
            _redis = redis;
            _db = redis.GetDatabase();
            _code = code;
            
            InitializeComponent();

            label1.Text = "Welcome " + code;
            var server = _redis.GetServer("localhost: 6379");
            foreach (var key in server.Keys(pattern: $"L-{code}-*"))
            {
                string date = key.ToString().Split('-')[2];
                string from = _db.ListGetByIndex(key, 0);
                string to = _db.ListGetByIndex(key, 1);
                richTextBox1.Text = $"{richTextBox1.Text}{date}\t{from}\t{to}\n";
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            WorkDay  wd = new WorkDay();
            wd.date = dateTimePicker1.Value.Date.ToShortDateString();
            wd.FromTime = dateTimePicker2.Value.ToShortTimeString();
            wd.ToTime = dateTimePicker3.Value.ToShortTimeString();

            DateTime d = new DateTime();

            RedisValue val = _db.ListGetByIndex($"L-{_code}-{wd.date}", 0);
            if (val != RedisValue.Null)
            {
                _db.ListSetByIndex($"L-{_code}-{wd.date}", 0, wd.FromTime);
                _db.ListSetByIndex($"L-{_code}-{wd.date}", 1, wd.ToTime);
                richTextBox1.Text = $"Please relog to see changes!\n{richTextBox1.Text}";
            }
            else
            {
                _db.ListRightPush($"L-{_code}-{wd.date}", wd.FromTime);
                _db.ListRightPush($"L-{_code}-{wd.date}", wd.FromTime);

                for (DateTime dt = dateTimePicker2.Value; dt < dateTimePicker3.Value; dt = dt.AddMinutes(30))
                {
                    Debug.WriteLine(dt.ToString());
                    _db.StringSet($"V-{_code}-{wd.date}-{dt.ToShortTimeString()}", "FREE");
                }

                _db.KeyExpire($"L-{_code}-{wd.date}", dateTimePicker1.Value.AddDays(1).ToLocalTime());
                richTextBox1.Text = $"{richTextBox1.Text}{wd.date}\t{wd.FromTime}\t{wd.ToTime}\n";
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            string date = dateTimePicker1.Value.Date.ToShortDateString();
            if (_db.KeyExists($"L-{_code}-{date}"))
            {
                _db.KeyDelete($"L-{_code}-{date}");

                var server = _redis.GetServer("localhost: 6379");
                foreach (var key in server.Keys(pattern: $"V-{_code}-{date}-*"))
                {
                    var transaction = _db.CreateTransaction();
                    transaction.AddCondition(Condition.KeyExists(key));
                    transaction.KeyDeleteAsync(key);
                    bool commited = transaction.Execute();
                    if (commited) Debug.WriteLine("Deleted: " + key.ToString());
                }


               richTextBox1.Text = $"Please relog to see changes!\n{richTextBox1.Text}";
            }
        }
    }
}
